<?php

namespace App\Helpers;

class JSend
{
    const STATUS_SUCCESS = 'success';
    const STATUS_FAIL = 'fail';
    const STATUS_ERROR = 'error';

    private $status;
    private $data;
    private $message;
    private $code;

    /**
     * @param string $status
     * @param array|null $data
     * @param string|null $message
     * @param string|null $code
     */
    private function __construct(
        string $status,
        array $data = null,
        string $message = null,
        string $code = null
    ) {
        $this->status = $status;
        $this->data = $data;
        $this->message = $message;
        $this->code = $code;
    }

    /**
     * @param array $data
     * @param string|null $message
     * @return JSend
     */
    public static function success(array $data, string $message = null)
    {
        return new self(self::STATUS_SUCCESS, $data, $message);
    }

    /**
     * @param array $data
     * @param string|null $message
     * @return JSend
     */
    public static function fail(array $data, string $message = null)
    {
        return new self(self::STATUS_FAIL, $data, $message);
    }

    /**
     * @param string $message
     * @param string $code
     * @param array|null $data
     * @return JSend
     */
    public static function error(string $message, string $code, array $data = null)
    {
        return new self(self::STATUS_ERROR, $data, $message, $code);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return array_filter([
            'status' => $this->status,
            'data' => $this->data,
            'message' => $this->message,
            'code' => $this->code,
        ], function ($value) {
            return !is_null($value);
        });
    }
}
