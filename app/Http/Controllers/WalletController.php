<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Zend\Soap\Client;

class WalletController extends Controller
{
    /**
     * @param Request $request
     * @param Client $client
     * @return JsonResponse
     * @throws ValidationException
     */
    public function recharge(Request $request, Client $client)
    {
        $input = $this->validate($request, [
            'document' => 'required',
            'mobilePhone' => 'required',
            'value' => 'required',
        ]);

        $response = $client->call('rechargeWallet', $input);

        return response()->json($response);
    }

    /**
     * @param Request $request
     * @param Client $client
     * @return JsonResponse
     * @throws ValidationException
     */
    public function checkBalance(Request $request, Client $client)
    {
        $input = $this->validate($request, [
            'document' => 'required',
            'mobilePhone' => 'required',
        ]);

        $response = $client->call('checkBalance', $input);

        return response()->json($response);
    }
}
