<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Zend\Soap\Client;

class PaymentController extends Controller
{
    /**
     * @param Request $request
     * @param Client $client
     * @return JsonResponse
     * @throws ValidationException
     */
    public function index(Request $request, Client $client)
    {
        $input = $this->validate($request, [
            'document' => 'required',
            'mobilePhone' => 'required',
            'value' => 'required',
        ]);

        $response = $client->call('payment', $input);

        return response()->json($response);
    }

    /**
     * @param Request $request
     * @param Client $client
     * @return JsonResponse
     * @throws ValidationException
     */
    public function confirm(Request $request, Client $client)
    {
        $input = $this->validate($request, [
            'sessionId' => 'required',
            'token' => 'required',
        ]);

        $response = $client->call('confirmPayment', $input);

        return response()->json($response);
    }
}
