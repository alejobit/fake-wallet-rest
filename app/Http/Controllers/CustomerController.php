<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Zend\Soap\Client;

class CustomerController extends Controller
{
    /**
     * @param Request $request
     * @param Client $client
     * @return JsonResponse
     * @throws ValidationException
     */
    public function registration(Request $request, Client $client)
    {
        $input = $this->validate($request, [
            'document' => 'required',
            'fullName' => 'required',
            'email' => 'required|email',
            'mobilePhone' => 'required',
        ]);

        $response = $client->call('customerRegistration', $input);

        return response()->json($response);
    }
}
