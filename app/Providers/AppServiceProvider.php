<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Zend\Soap\Client;

class AppServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton(Client::class, function () {
            return new Client(env('FAKE_WALLET_SOAP_WSDL'));
        });
    }
}
