## Fake Wallet REST Service

### Installation

#### Using Docker Compose

```
git clone git@bitbucket.org:alejobit/fake-wallet-rest.git
cd fake-wallet-rest
cp .env.example .env
docker-compose up -d
docker-compose exec php-fpm composer install
```
