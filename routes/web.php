<?php

/** @var Router $router */

use Laravel\Lumen\Routing\Router;

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->post('/customer/registration', 'CustomerController@registration');

$router->post('/wallet/recharge', 'WalletController@recharge');
$router->get('/wallet/check-balance', 'WalletController@checkBalance');

$router->post('/payment', 'PaymentController@index');
$router->post('/payment/confirm', 'PaymentController@confirm');
